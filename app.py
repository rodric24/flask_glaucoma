from flask import Flask, request,render_template, json
# import pickle
import pandas as pd
import os
import cv2
import numpy as np
from keras.models import load_model
from PIL import Image
from skimage.transform import resize

app = Flask(__name__)

@app.route('/',methods=["Get","POST"])
def home():
    return render_template("index.html")

@app.route('/predict',methods=["Get","POST"])
def predict():
    new_file = request.files['file']
    target_path = os.path.join("upload",new_file.filename)
    new_file.save(target_path)

    list_to_tensor = []
    image = cv2.imread(target_path, 0)
    image = image.flatten()
    image = resize(np.array(Image.open(target_path)), (224, 224))
    list_to_tensor.append(image)

    classifier = load_model("ml/model_simple.h5")
    prediction = classifier.predict(np.array(list_to_tensor))
    print(prediction)

    return f"Resultado de la predicción: {prediction.tolist()[0]} ;) !!"

def data_validation(image):
    image = image.flatten()
    image = np.expand_dims(image, axis=0)
    with open("ml/scaler.pkl", 'rb') as file:
        scaler = pickle.load(file)
        image = scaler.transform(image)
    return image

if __name__ == '__main__':
     app.run(debug=True, port=8081)