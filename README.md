# FLASK_PAPILA
## INTEGRANTES
* Jhoan Chiri Maldonado 100%
* Lizette Isabel Andrea Gemio Duran 100%
* Rodrigo Condori Quisbert 100%
* Federico Ramirez Vinaya 100%
## DESCRIPCION DEL PROYECTO
Proyecto API REST basado en PYTHON/FLASK para levantar una pagina y servicios para la prediccion de imagenes del proyecto: *PAPILA: CONJUNTO DE DATOS CLÍNICOS DE AMBOS OJOS PARA EVALUACIÓN DE PACIENTES CON GLAUCOMA*

El endpoint /predict se encarga de realizar la prediccion a partir de un modelo keras en formato *.H5*

## SOBRE EL EMPAQUETADO CON DOCKER
Como se ve en el video el presente proyecto esta listo para generar una imagen con las dependencias necesarias con un contenedor en DOCKER. Para lo cual se siguen los siguientes comandos:
```sh
# Crear una imagen
$ sudo docker build -t flask_papila .
# Levantar un contenedor a partir de la imagen creada
$ sudo docker run -dp 8081:5002 -ti --name flask_papila flask_papila
```
